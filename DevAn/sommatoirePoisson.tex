\begin{definition}Pour tout $F\in L^1(\R)$, on note $\widehat F: \anonymfunction{\R}{\R}{x}{\int_{-\infty}^{+\infty}e^{-2i\pi xt}F(t)dt}$.\end{definition}

\begin{thm}Soit $F\in L^1(\R)\cap\mathcal C^0(\R)$ telle que :
	\begin{itemize}
		\item il existe $M>0$ et $\alpha>1$ tels que : $\forall x\in\R, \; \abs{F(x)} \le \frac{M}{(1+\abs x)^\alpha}$ ;
		\item $\sum_{n=-\infty}^{+\infty}\abs{\widehat F(x)} < +\infty$.
	\end{itemize}
	Alors :
	\begin{align}\label{eq:poisson1}
		\sum_{n=-\infty}^{+\infty} F(n) = \sum_{n=-\infty}^{+\infty} \widehat F(n)
	\end{align}
\end{thm}

\begin{proof}\begin{enumerate}
	\item Montrons que la série double $x\mapsto\sum F(x+n)$ est normalement convergente sur les compacts de $\R$. Soient $A>0, x\in\intervalCC{-A}{A}$ et $n\in\Z$ tel que $\abs n\ge2A$. Alors, $\abs{x+n}\ge\abs n-\abs x\ge \abs n-A\ge\frac n2$, donc :
	\begin{align*}
		\abs{F(x+n)} \le \frac{M}{(1+\abs x)^\alpha} \le \frac{M}{\left(1+\frac{\abs n}{2}\right)^\alpha}
	\end{align*}
	Or, $\sum\frac{M}{\left(1+\frac{\abs n}{2}\right)^\alpha}$ est convergente puisque $\alpha>1$. Donc $x\mapsto\sum F(x+n)$ converge normalement sur $\intervalCC{-A}{A}$.
	\item Soit $f:\anonymfunction{\R}{\R}{x}{\sum_{n=-\infty}^{+\infty}F(x+n)}$. Comme $F$ est continue, $f$ aussi. Par ailleurs,
	\begin{align*}
		\forall x\in\R, \; f(x+1) = \sum_{n=-\infty}^{+\infty}F(x+1+n) = \sum_{p=-\infty}^{+\infty}F(x+p) = f(x)
	\end{align*}
	Donc $f$ est $1$-périodique.
	\item Soit $m\in\Z$. On calcule :
	\begin{align*}
		c_m(f) = \int_0^1 f(t)e^{-2i\pi mt}dt = \int_0^1\sum_{n=-\infty}^{+\infty}F(x+n)e^{-2i\pi mt}dt
	\end{align*}
	$\intervalCC01$ est compact donc $\sum F(x+n)$ converge normalement, et $\abs{e^{-2i\pi mt}} = 1$ pour tout $t\in\intervalCC01$. Donc :
	\begin{align*}
		c_m(f) &= \sum_{n=-\infty}^{+\infty}\int_0^1F(x+n)e^{-2i\pi mt}dt = \sum_{n=-\infty}^{+\infty}\int_0^1F(t+n)e^{-2i\pi(t+n)m}dt\\
		       &= \sum_{n=-\infty}^{+\infty}\int_{n}^{n+1}F(u)e^{-2i\pi mu}du = \int_{-\infty}^{+\infty}F(u)e^{-2i\pi mu}du = \widehat F(m)
	\end{align*}
	\item Par conséquent, $\sum_{m=-\infty}^{+\infty}\abs{c_m(f)} = \sum_{m=-\infty}^{+\infty}\abs{\widehat F(m)} < +\infty$, par hypothèse. Or, $f\in\mathcal C^0(\intervalCC01)$ et $f$ est $1$-périodique, donc :
	\begin{align*}
		\forall x\in\R, \; f(x) = \sum_{m=-\infty}^{+\infty}c_m(f)e^{2i\pi mx} = \sum_{m=-\infty}^{+\infty} \widehat F(m)e^{2i\pi mx}
	\end{align*}
	Ainsi :
	\begin{align*}
		\forall x\in\R, \; \sum_{n=-\infty}^{+\infty}F(x+n) = \sum_{m=-\infty}^{+\infty}\widehat F(m)e^{2i\pi mx}
	\end{align*}
	En $x=0$, la conclusion découle.
\end{enumerate}\end{proof}

\begin{appl}On a :
	\begin{align*}
		\forall a>0, \; \sum_{n=-\infty}^{+\infty}\frac{1}{a^2+n^2} = \frac\pi a\coth(\pi a)
	\end{align*}
\end{appl}

\begin{proof}\begin{itemize}
	\item Soit $f:\anonymfunction{\R}{\R}{t}{e^{-2\pi a\abs t}}$ Comme $f\in L^1(\R)$,
	\begin{align*}
		\forall x\in\R, \; \widehat f(x) = \int_{-\infty}^{+\infty}e^{2-i\pi tx}f(t)dt
	\end{align*}
	Soit $x\in\R$. On calcule :
	\begin{align*}
		\int_{-\infty}^{+\infty} e^{-2i\pi tx}f(t)dt = \int_{-\infty}^{0}e^{-2i\pi tx}e^{2\pi at}dt + \int_{0}^{+\infty}e^{-2i\pi tx}e^{-2\pi at}dt
	\end{align*}
	On a :
	\begin{align*}\left\{\begin{aligned}
		&\int_{-\infty}^{0}e^{2\pi(a-ix)t}dt = \left[\frac{1}{2\pi(a-ix)}\right]_{-\infty}^0 = \frac{1}{2\pi(a-ix)}\\
		&\int_{0}^{+\infty}e^{-2\pi(a+ix)}dt = \left[\frac{-1}{2\pi(a+ix)}\right]_0^{+\infty} = \frac{1}{2\pi(a+ix)}
	\end{aligned}\right.,\end{align*}
	car :
	\begin{align*}\left\{\begin{aligned}
		&\abs{\frac{1}{2\pi(a-ix)}e^{2\pi(a-ix)t}} \xrightarrow[t\to-\infty]{}0\\
		&\abs{\frac{-1}{2\pi(a+ix)}e^{-2\pi(a+ix)t}} \xrightarrow[t\to+\infty]{}0
	\end{aligned}\right.\end{align*}
	Donc :
	\begin{align*}
		\int_{-\infty}^{+\infty}e^{-2i\pi tx}f(t)dt = \frac{1}{2\pi}\left(\frac{1}{a-ix} + \frac{1}{a+ix}\right) = \frac{a}{\pi(a^2+x^2)}
	\end{align*}
	\item D'après la formule sommatoire de Poisson,
	\begin{align*}
		\frac a\pi\sum_{n=-\infty}^{+\infty}\frac{1}{a^2+n^2} &= \sum_{n=-\infty}^{+\infty}e^{-2\pi a\abs n} = 2\sum_{n=0}^{+\infty} e^{-2\pi an} -1\\
		  &= \frac{2}{1-e^{-2\pi a}} - 1 = \frac{1+e^{-2\pi a}}{1-e^{-2\pi a}} = \coth(\pi a)
	\end{align*}
\end{itemize}\end{proof}
