\begin{definition}[Fonction de poids]Soit $I\subset\R$ un intervalle. On appelle fonction de poids sur $I$ une fonction mesurable $\rho:I\to\R^{+*}$, telle que :
	\begin{align*}
		\forall n\in\N, \; \int_I \abs x^n\rho(x)dx < +\infty
	\end{align*}
\end{definition}

\begin{thm}Soit $\rho:I\to\R$ une fonction de poids. On suppose qu'il existe $\alpha\in\R^{+*}$ tel que :
	\begin{align}\label{eq:DensitePolOrthohyp}
		\int_I e^{\alpha\abs x}\rho(x)dx < +\infty
	\end{align}
Alors, la famille des polynômes $(g_n:x\mapsto x^n)_{n\in\N}$ est dense dans $L^2(I, \rho)$ pour le produit scalaire $\scalPdt{\cdot}{\cdot}_\rho$ associé à $\rho$.\end{thm}

\begin{proof}Pour tout $n\in\N$, on pose $g_n:\anonymfunction{I}{\R}{x}{x^n}$.
	\begin{enumerate}
		\item Soit $\rho$ une fonction de poids quelconque, ne vérifiant pas nécessairement \eqref{eq:DensitePolOrthohyp}.  Soient $n\in\N$ et $p\in\intervalCO{1}{+\infty}$. Supposons que $g_n\in L^1(I, \rho)$ et montrons que $g_n\in L^p(I, \rho)$. Comme $\rho$ est une fonction de poids,
		\begin{align*}
			\int_I \abs{x}^{np}\rho(x) dx \le\int_I\left(1 + \abs{x}^{\integerPart{np}+1}\right)\rho(x)dx < +\infty
		\end{align*}
	Donc $g_n\in L^p(I, \rho)$.
	\item Jusqu'à la fin de la démonstration, on suppose \eqref{eq:DensitePolOrthohyp} satisfaite. Soient $f\in L^2(I, \rho)$ et $\varphi:\R\to\R$ définie par :
	\begin{align*}
		\forall x\in\R, \; \varphi(x) = \begin{cases}
			f(x)\rho(x) & \text{si } x\in I\\
			0           & \text{sinon}
		\end{cases}
	\end{align*}
	\begin{itemize}
		\item Montrons que $\varphi\in L^1(\R)$. Pour ce faire, on remarque que pour tout $t\in \R, t \le \frac12(1+t^2)$, donc :
		\begin{align*}
			\forall x\in I, \; \abs{f(x)}\rho(x) \le \frac12 \left(1 + \abs{f(x)}^2\right)\rho(x)
		\end{align*}
		Or, $\rho\in L^1(\R)$ et $f\in L^2(I)$ donc $\rho f^2\in L^1(I)$. On en déduit que $\varphi\in L^1(\R)$.
		\item Il s'ensuit que la transformée de Fourier $\widehat\varphi:\R\to\C$ de $\varphi$ est bien définie, et que :
		\begin{align*}
			\forall\omega\in\R, \; \widehat\varphi(\omega) = \int_I f(x)e^{-i\omega x}\rho(x)dx
		\end{align*}
		\item Soit $B_\alpha = \setst{z\in\C}{\abs{\Im z} < \frac\alpha2}$. Montrons que $\widehat\varphi$ se prolonge en une fonction holomorphe sur $B_\alpha$. On considère $g:\anonymfunction{\C\times\R}{\C}{(z, x)}{e^{-izx}f(x)\rho(x)}$. Soient $z\in B_\alpha$ et $x\in\R$. Alors :
		\begin{align*}
			\abs{g(z, x)} = \abs{e^{-izx}f(x)\rho(x)} = \abs{e^{-izx}}\cdot\abs{f(x)}\rho(x) \le e^{\alpha\frac{\abs x}{2}}\cdot\abs{f(x)}\cdot\rho(x)
		\end{align*}
		En intégrant, on déduit :
		\begin{align*}
			\int_I\abs{g(z, x)}dx &\le \int_Ie^{\alpha\frac{\abs x}{2}}\cdot\abs{f(x)}\cdot\rho(x)dx\\
				&\stackrel{\text{C.-S.}}{\le} \underbrace{\left(\int_I e^{\alpha\abs x}\rho(x)dx\right)^{1/2}}_{<+\infty \text{ par } \eqref{eq:DensitePolOrthohyp}} \underbrace{\left(\int_I\abs{f(x)}^2\rho(x)dx\right)^{1/2}}_{<+\infty \text{ car } f\in L^2(I, \rho)}
		\end{align*}
		On définit $F: \anonymfunction{B_\alpha}{\C}{z}{\int_I g(z, x)dx = \int_I f(x)e^{-izx}\rho(x)dx}$. On a démontré que $F$ est bien définie. Montrons que $F\in\mathcal H(B_\alpha)$.
		\begin{itemize}
			\item Pour tout $z\in B_\alpha$, $\anonymfunction{I}{\C}{x}{g(z, x)}$ est mesurable.
			\item Pour presque tout $x\in I$, $\anonymfunction{B_\alpha}{\C}{z}{g(z, x)}$ est holomorphe sur $B_\alpha$.
			\item Si $z\in\C$ est tel que $\abs{\Im(z)}\le\frac\alpha2$, alors $\abs{g(z, x)}\le e^{\alpha\frac{\abs x}{2}}\cdot\abs{f(x)}\cdot\rho(x) \in L^1(I)$.
		\end{itemize}
		Par conséquent, le théorème d'holomorphie sous l'intégrale s'applique et montre que $F$ est holomorphe sur $B_\alpha$.
	\end{itemize}
	\item Soient $n\in\N$ et $z\in B_\alpha$. Alors :
	\begin{align*}
		F^{(n)}(z) &= \int_I \frac{\partial^n}{\partial z^n}(e^{-izx}f(x)\rho(x))dx\\
		  &= (-i)^n\int_I x^ne^{-izx}f(x)\rho(x)dx
	\end{align*}
	En particulier,
	\begin{align*}
		F^{(n)}(0) = (-i)^n\int_I x^nf(x)\rho(x)dx = (-i)^n\scalPdt{f}{g_n}_\rho
	\end{align*}
	Si maintenant $\scalPdt{f}{g_n}_\rho = 0$, comme $n$ est arbitraire, on en déduit que $F$ est nulle au voisinage de $0$, et par principe d'identité, comme $B_\alpha$ est simplement connexe, il s'ensuit que $F$ est nulle sur $B_\alpha$. En particulier, $F_{|\R} = \widehat\varphi \equiv 0$. Par injectivité de $\widehat\cdot$, on déduit que $\varphi \equiv 0$, donc $f\equiv0$ presque partout sur $I$. Ainsi, $((g_n)_{n\in\N})^{\perp_\varphi} = \{0\}$, donc la suite $(g_n)_{n\in\N}$ est dense dans $L^2(I, \rho)$.
	\end{enumerate}
\end{proof}
