Soit $(\Omega, \mathcal A, P)$ un espace probabilisé.

\begin{lemme}Soient $X$ une variable aléatoire réelle de loi $P_X$ et $\varphi_X:\anonymfunction{\R}{\C}{t}{\int_\R e^{itx}dP_X(x)}$ sa fonction caractéristique. Si $X\in L^p(\Omega)$ pour $p\in\N^*$, alors :
	\begin{align}\label{eq:lem}
		\left\{\begin{aligned}
			&\varphi_X\in\mathcal C^p(I)\\
			&\forall k\in\integerInterval 0p, \partial^k\varphi_X(0) = i^kE(X^k)
		\end{aligned}\right.
	\end{align}
	En particulier, si $X\in L^2(\Omega)$, alors :
	\begin{align*}
		\forall t\in\R, \; \varphi_X(t) = 1 + tiE(X) - \frac{t^2}{2}E(X^2) + t^2\varepsilon(t),
	\end{align*}
	avec $\lim_{t\to0}\varepsilon(t) = 0$.
\end{lemme}

\begin{proof}\begin{itemize}
	\item Soit $f:\anonymfunction{\R\times\R}{\C}{(t, x)}{e^{itx}}$. Alors :
	\begin{enumerate}
		\item Pour tout réel $t$, $\anonymfunction{\R}{\C}{x}{e^{itx}}$ est continue donc  mesurable.
		\item Pour tout réel $x$, $\anonymfunction{\R}{\C}{t}{e^{itx}}$ est $\mathcal C^p$ et :
		\begin{align*}
			\forall k\in\integerInterval 0p, \; \abs{\frac{\partial^k}{\partial t^k}f(t, x)} = \abs{(it)^ke^{itx}} \le \abs x^k \in L^p\subset L^k
		\end{align*}
	\end{enumerate}
	D'après le théorème de dérivabilité $\mathcal C^k$ de Lebesgue, on déduit :
	\begin{align*}
		\forall t\in\R, \forall k\in\integerInterval 0p, \frac{\partial^k}{\partial t^k}\varphi_X(t) = \int_\R(ix)^ke^{itx}dP_X(x)
	\end{align*}
	\item En particulier, pour tout $k\in\integerInterval 0p$,
	\begin{align*}
		\frac{\partial^k}{\partial t^k}\varphi_X(0) = \int_\R i^kx^kdP_X(x)= i^kE(X^k)
	\end{align*}
	Et donc, si $X\in L^2(\Omega)$, on trouve la formule \eqref{eq:lem}
\end{itemize}\end{proof}

\begin{thm}[central limite] Soit $(X_n)_{n\in\N}$ une suite de variables aléatoires réelles i.i.d. dans $L^2(\Omega)$, d'espérance $m$ et de variance $\sigma^2>0$. La suite $(Y_n)_{n\in\N^*} = \left(\frac{\sqrt n}{\sigma}\frac{X_1+…+X_n}{n} - m\right)_{n\in\N^*}$ converge en loi vers $\mathcal N(0, 1)$.\end{thm}

\begin{proof}\begin{itemize}
	\item Soit $n\in\N^*$. Par définition, $Y_n = \frac{1}{\sigma\sqrt n}(X_1-m + … + X_n-m)$. Soit $\varphi$ la fonction caractéristique commune des $X_i-m, i\in\N$. D'après le lemme, $\varphi\in\mathcal C^2(\R)$, et quel que soit $t\in\R$, \eqref{eq:lem} donne :
	\begin{align}\label{eq:etap1}
		\varphi(t) = 1 + tiE(X_1-m) - \frac{t^2}{2}E((X_1-m)^2) + t^2\varepsilon(t) = 1 - \frac{t^2}{2}\sigma^2 + t^2\varepsilon(t),
	\end{align}
	avec $\lim_{t\to0}\varepsilon(t) = 0$.
	\item Comme les $X_i-m, i\in\N$ sont indépendants, quels que soient $n\in\N^*$ et $t\in\R$,
	\begin{align*}
		\varphi_{Y_n}(t) &= E\left(e^{i\frac{t}{\sigma\sqrt n}(X_1-m + … + X_n-m}\right)\\
		  &= E\left(e^{i\frac{t}{\sigma\sqrt n}(X_1-m)}\right) \cdot … \cdot E\left(e^{i\frac{t}{\sigma\sqrt n}(X_n-m)}\right)\\
		  &= \left(\varphi\left(\frac{t}{\sigma\sqrt n}\right)\right)^n\\
		  &\stackrel{\eqref{eq:etap1}}{=}\left(1 - \frac 12\left(\frac{t}{\sigma\sqrt n}\right)^2\sigma^2 + \left(\frac{t}{\sigma\sqrt n}\right)^2 \varepsilon\left(\frac{t}{\sigma\sqrt n}\right)\right)^n\\
		  &= \left(1 - \frac12\frac{t^2}{n} + \frac1n\frac{t^2}{\sigma^2}\varepsilon\left(\frac{t}{\sigma\sqrt n}\right)\right)^n
	\end{align*}
	Pour $n\in\N$ et $t\in\R$, on pose $u_n(t) = -\frac{t^2}{2} + \left(\frac t\sigma\right)^2\varepsilon\left(\frac{t}{\sigma\sqrt n}\right)$, de sorte que $\varphi_{Y_n}(t) = \left(1 + \frac{u_n(t)}{n}\right)^n$.
	\item Soit $t\in\R$. Comme $\lim_{n\to+\infty}u_n(t) = -\frac{t^2}{2}$, on obtient $\lim_{n\to+\infty}\left(1 + \frac{u_n(t)}{n}\right)^n = e^{-t^2/2}$. Donc :
	\begin{align}\label{eq:etap2}
		\forall t\in\R, \; \lim_{n\to+\infty}\varphi_{Y_n}(t) = e^{-t^2/2}
	\end{align}
	Or, $\anonymfunction{\R}{\R}{t}{e^{-t^2/2}}$ est la fonction caractéristique de la loi $\mathcal N(0, 1)$. D'après le théorème de Lévy, \eqref{eq:etap2} suffit à conclure que $\converge{Y_n}{n}{+\infty}{\text{loi}}{\mathcal N(0, 1)}$.
\end{itemize}\end{proof}
