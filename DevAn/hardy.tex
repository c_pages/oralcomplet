\begin{thm}[de Hardy]Soit $(c_n)_{n\in\N}\in\R^\N$ telle que $\sum c_n$ converge. Alors, la fonction $g: \anonymfunction{\R^+}{\R}{t}{\sum_{n=0}^{+\infty}\frac{c_n}{n!}t^n}$ est bien définie et est continue. Soit :
	\begin{align*}
		\forall x\in\R^+, \; I(x) = \int_0^x g(t)e^{-t}dt
	\end{align*}
$I$ est bien définie, continue sur $\R^+$, admet une limite lorsque $x\to+\infty$, et :
	\begin{align*}
		\lim_{x\to+\infty}I(x) = \int_0^{+\infty}g(t)e^{-t}dt = \sum_{n=0}^{+\infty}c_n
	\end{align*}
\end{thm}

\begin{proof}\begin{enumerate}
	\item La suite $(c_n)_{n\in\N}$ est bornée, donc il existe $M\in\R^+$ tel que :
	\begin{align*}
		\forall n\in\N, \; 0\le \frac{\abs{c_n}}{n!} \le \frac{M}{n!}
	\end{align*}
Donc, pour tout $t\in\R^+$,
	\begin{align*}
		\sum_{n=0}^{+\infty}\frac{\abs{c_n}t^n}{n!} \le M\sum_{n=0}^{+\infty} \frac{t^n}{n!}
	\end{align*}
Donc $\sum\frac{c_n}{n!}t^n$ est une série entière de rayon de convergence $+\infty$, donc $g$ est de classe $\mathcal C^{\infty}$, et en particulier continue, sur $\R^+$.
	\item Soit $x\in\R^+$. Comme $\anonymfunction{\intervalCC 0 x}{\R}{t}{g(t)e^{-t}}$ est continue (donc bornée), $I(x)$ est bien définie et appartient à $\R$. Par définition,
	\begin{align*}
		I(x) = \int_0^x \sum_{n=0}^{+\infty}\frac{c_n}{n!}t^n e^{-t}dt
	\end{align*}
	Or :
	\begin{itemize}
		\item $\sum\frac{c_n}{n!}t^n$ est une série entière de rayon de convergence $+\infty$, donc $\sum_{n=0}^{+\infty}\frac{c_n}{n!}t^n$ converge uniformément sur tout compact de $\intervalCC 0 x$.
		\item $\anonymfunction{\R^+}{\R}{t}{e^{-t}}$ est bornée sur $\intervalCC 0 x$.
	\end{itemize}
	On peut donc échanger $\int$ et $\sum$. En raisonnant par I.P.P. et en itérant, on obtient :
	\begin{align*}
		I(x) &= \sum_{n=0}^{+\infty}\frac{c_n}{n!}\int_0^xt^ne^{-t}dt\\
		     &= \sum_{n=0}^{+\infty}c_n\left(1-e^{-x}\sum_{k=0}^n\frac{x^k}{k!}\right)
	\end{align*}
	On note, pour tout $n\in\N$, $I_n(x) = 1-e^{-x}\sum_{k=0}^n\frac{x^k}{k!}$. Ainsi :
	\begin{align*}
		I(x) = \sum_{n=0}^{+\infty}c_nI_n(x)
	\end{align*}
	\item Quels que soient $x\in\R^+$ et  $n\in\N$, on remarque que $0\le I_{n+1}(x)\le I_n(x) \le 1$. Montrons, grâce au critère de Cauchy uniforme, que $x\mapsto\sum c_nI_n(x)$ est une série de fonctions uniformément convergente. Si $n\in\N$, on note
	\begin{align*}
		R_n = \sum_{k=n}^{+\infty}c_n
	\end{align*}
Puis, pour $(p, q)\in\N^2$ tel que $p>q$ et $x\in\R^+$, on définit $\Delta_{p, q}(x)$ par :
	\begin{align*}
		\Delta_{p, q}(x) &= \sum_{k=p}^q c_kI_k(x)\\
		                 &= \sum_{k=q}^p (R_k - R_{k+1})I_k(x)\\
		                 &= \sum_{k=q+1}^p \left(R_k\cdot (I_k(x) - I_{k-1}(x)\right) + R_q\cdot I_q(x) - R_{p+1}\cdot I_p(x)
	\end{align*}
Soit $x\in\R^+$. Il suffit de montrer que $\Delta_{p, q}(x)$ tend vers $0$ uniformément en $x$ lorsque $p, q\to+\infty$. Soient $\varepsilon\in\R^{+*}$ et  $n_0\in\N$ tel que si $n>n_0$, $\abs{R_n} \le \varepsilon$. On calcule, quels que soient $x\in\R^+$ et $p>q>n_0$,
	\begin{align*}
		\abs{\Delta_{p, q}(x)} &\le \varepsilon \left(\sum_{k=q+1}^p \left((I_k(x) - I_{k-1}(x)\right) + I_q(x) - I_p(x)\right)\\
		  &\le \varepsilon(I_q(x) - I_p(x) + I_q(x) + I_p(x))\\
		  &\le 2\varepsilon
	\end{align*}
	D'où convergence uniforme de la série de fonctions $\sum c_nI_n$.
	\item Quel que soit $n\in\N$,
	\begin{align*}
		\lim_{x\to+\infty}I_n(x) = 1
	\end{align*}
	De plus, $x\mapsto \sum c_nI_n(x)$ converge uniformément. Par interversion de limite, on en déduit que $\sum_{n=0}^{+\infty}c_nI_n(x)$ admet une limite lorsque $x\to+\infty$, et :
	\begin{align*}
		\lim_{x\to+\infty} \sum_{n=0}^{+\infty}c_nI_n(x) &= \sum_{n=0}^{+\infty}\lim_{x\to+\infty}c_nI_n(x)\\
		\text{i.e}\quad \lim_{x\to+\infty}\int_0^x g(t)e^{-t}dt &= \sum_{n=0}^{+\infty}c_n\lim_{x\to+\infty}I_n(x)\\
		\text{i.e}\quad \int_0^{+\infty} g(t)e^{-t}dt &= \sum_{n=0}^{+\infty}c_n
	\end{align*}
\end{enumerate}

\end{proof}
