\begin{lemme}\label{lem:inegHoeffding}Soient $(\Omega, \mathcal A, \P)$ un espace probabilisé, $X$ une variable aléatoire centrée, bornée par $1$. Alors :
	\begin{align*}
		\forall t\in\R, \; \E(e^{tX}) \le e^{\frac{t^2}{2}}
	\end{align*}
\end{lemme}

\begin{proof}\begin{itemize}
	\item On  remarque que : 
	\begin{align*}
		\forall t\in\R, \forall x\in\intervalCC{-1}{1}, \; tx = \frac12(1+x)t + \frac12(1-x)t
	\end{align*}
	Donc, par convexité de $\exp$, on déduit que :
	\begin{align}\label{eq:inegHoeffding_1}
		\forall t\in\R, \forall x\in\intervalCC{-1}{1}, \; e^{tx}\le\frac12(1-x)e^{-t}+\frac12(1+x)e^t
	\end{align}
	\item Soit $t\in\R$. Comme $\abs X\le1$ $\P$-p.s., $e^{tX}\le e^t$ $\P$-p.s., donc $e^{tX}$ admet une espérance. D'après \eqref{eq:inegHoeffding_1},
	\begin{align*}
		e^{tX} \le \frac12(1-X)e^{-t} + (1+X)e^t
	\end{align*}
	Par linéarité de l'espérance, $\E(e^{tX}) \le \frac12e^{-t}\E(1-X) + \frac12e^t\E(1+X)$. Comme $X$ est centrée, il vient $\E(e^{tX}) \le \cosh(t)$.
	\item Fixons $t\in\R$. Alors :
	\begin{align*}
		\cosh(t) = \sum_{n=0}^{+\infty}\frac{t^{2n}}{(2n)!} \le \sum_{n=0}^{+\infty}\frac{t^{2n}}{2^nn!} = e^{\frac{t^2}{2}}
	\end{align*}
	Ainsi, $E^{tX}\le e^{\frac{t^2}{2}}$, ce qui démontre le lemme \ref{lem:inegHoeffding}.
\end{itemize}\end{proof}

\begin{thm}[Inégalité de Hoeffding]Soit $(X_n)_{n\in\N^*}$ une suite de v.a.r. indépendantes centrées, telles que pour tout $n\in\N^*$, il existe $c_n\in\R^*$ vérifiant $\abs{X_n}\le c_n$. Pour $n\in\N^*$, soit $S_n = \sum_{j=0}^n X_j$. Alors :
	\begin{align}\label{thm:inegHoeffding}
		\forall n\in\N^*, \forall\varepsilon\in\R^{+*}, \; \P(\abs{S_n} > \varepsilon) \le e^{-\frac{\varepsilon^2}{2\sum_{j=1}^n c_j^2}}
	\end{align}
\end{thm}

\begin{proof}\begin{itemize}
	\item Montrons que : $\forall n\in\N^*, \forall t\in\R, \; \E(e^{tS_n}) \le e^{\frac{t^2}{2}\sum_{j=1}^,c_j^2}$.
	
	Soit $n\in\N^*$. Appliquons le lemme \ref{lem:inegHoeffding} à $\frac{X_n}{c_n}$ :
	\begin{align*}
		\forall t'\in\R, \; \E\left(e^{t'\frac{X_n}{c_n}}\right) \le e^{\frac{t'^2}{2}}
	\end{align*}
	D'où $\E(e^{tX_n}) \le e^{\frac{t^2}{2}c_n^2}$. Comme les $X_j$ où $j\in\integerInterval 1n$ sont indépendants, on déduit :
	\begin{align}\label{eq:inegHoeffding_2}
		\E\left(e^{t\sum_{j=1}^n}X_j\right) = \prod_{j=1}^n\E\left(e^{tX_j}\right) \le e^{\frac{t^2}{2}\sum_{j=1}^n c_j^2}
	\end{align}
	\item Fixons $\varepsilon\in\R^{+*}$ et $t\in\R^{+*}$. Soit $n\ge1$ un entier. Par croissance de la fonction $\exp$, $\{S_n>\varepsilon\}\subset\{e^{tS_n} > e^{t\varepsilon}\}$. D'après l'inégalité de Markov,
	\begin{align*}
		\P(S_n>\varepsilon) &\le \P(e^{tS_n} > e^{tx})\\
		                    &\le \frac{\E(e^{tS_n})}{e^{t\varepsilon}}\\
		                    &\stackrel{\eqref{eq:inegHoeffding_2}}{\le} e^{-t\varepsilon + \frac{t^2}{2}\sum_{j=1}^n c_j^2}
	\end{align*}
	\item Notons $a_n = \sum_{j=0}^n c_j^2$. L'application $\anonymfunction{\R}{\R}{t}{\frac a2t^2-t\varepsilon}$ atteint son minimum en $t=\frac{\varepsilon}{a_n}$, et ce minimum vaut $\frac{-\varepsilon^2}{2a_n}<0$. Par conséquent :
	\begin{align}\label{eq:inegHoeffding_3}
		\P(S_n>\varepsilon) \le e^{\frac{-\varepsilon^2}{2a_n}}
	\end{align}
	\item On écrit :
	\begin{align*}
		\{\abs{S_n} > \varepsilon\} &= \{S_n>\varepsilon\}\cup\{S_n<-\varepsilon\}\\
		                            &= \{S_n>\varepsilon\}\cup\{-S_n>\varepsilon\}
	\end{align*}
	Donc :
	\begin{align*}
		\P(\abs{S_n}>\varepsilon) \le \P(S_n>\varepsilon) + \P(-S_n>\varepsilon)
	\end{align*}
	Or, l'inégalité \eqref{eq:inegHoeffding_3} appliquée à $-S_n$ assure que :
	\begin{align*}
		\P(-S_n>\varepsilon) \le e^{\frac{-\varepsilon^2}{2a_n}}
	\end{align*}
	Ainsi, on obtient l'inégalité \eqref{thm:inegHoeffding} attendue :
	\begin{align*}
		\P(\abs{S_n}>\varepsilon) \le 2e^{\frac{-\varepsilon^2}{2\sum_{j=1}^nc_j^2}}
	\end{align*}
\end{itemize}\end{proof}
