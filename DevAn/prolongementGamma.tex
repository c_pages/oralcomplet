\begin{definition}Soient $\omega = \setst{z\in\C}{\Re z>0}$ et $\Gamma:\anonymfunction{\omega}{\C}{x}{\int_0^{+\infty}t^{x-1}e^{-t}dt}$.\end{definition}

\begin{thm}\label{thm:prolongementGamma}La fonction $\Gamma$ se prolonge en une fonction holomorphe sur $\C\setminus \Z^-$, avec des pôles simples sur $\Z^-$.\end{thm}

\begin{lemme}[formule d'Euler]$\forall z\in\omega, \; \Gamma(z) = \lim_{n\to+\infty}\frac{n^zn!}{z(z+1)…(z+n)}$.\end{lemme}

\begin{proof}Soit $z\in\omega$.
	\begin{enumerate}
		\item Soient $n\in\N$ et $f_n:\anonymfunction{\R^+}{\C}{t}{\indic_{\intervalOO 0n}(t)\left(1-\frac tn\right)^nt^{z-1}}$. Alors :
		\begin{enumerate}
			\item $(f_n)_{n\in\N}$ converge simplement vers $t\mapsto\indic_{\intervalOO{0}{+\infty}}(t)e^{-t}t^{z-1}$.
			\item Notons $x = \Re z>0$. Si $u\in\intervalCC01$, alors $1-u\le e^{-u}$. Donc, pour tout $t>0$ et pour tout $n\in\N$, $\abs{f_n(t)} \le \indic_{\intervalOO0n}(t)(e^{-t/n})^nt^{x-1} \le \indic_{\intervalOO{0}{+\infty}}(t)t^{x-1}e^{-t}$.
		\end{enumerate}
		D'après le théorème de convergence dominée, on conclut :
		\begin{align*}
			\Gamma(z) &= \int_0^{+\infty}t^{z-1}e^{-t}dt\\
			          &= \lim_{n\to+\infty}\int_0^n t^{z-1}\left(1-\frac tn\right)^ndt\\
			          &= \lim_{n\to+\infty}n^z\underbrace{\int_0^1 s^{z-1}(1-s)^nds}_{\stackrel{\text{def.}}{=} I_n(z)}
		\end{align*}
		\item Démontrons par récurrence sur $n\in\N$, que :
		\begin{align}\tag{$H_n$}
			I_n(z) = \frac{n!}{z(z+1)…(z+n)}
		\end{align}
		Bien sûr, $(H_0)$ est vraie. Soit $n\in\N$ et supposons $(H_n)$. Calculons :
		\begin{align*}
			I_{n+1}(z) &= \int_0^1 s^{z-1}(1-s)^{n+1}ds\\
			           &= \left[\frac{s^z}{s}(1-s)^{n+1}\right]_0^1 + \int_0^1\frac{s^z}{z}(n+1)(1-s)^nds\\
			           &= 0 + \frac{n+1}{z}\int_0^1s^{(z+1)-1}(1-s)^nds\\ 
			           &= 0 + \frac{(n+1)n!}{z(z+1)…(z+n+1)}
		\end{align*}
	\end{enumerate}
D'où la formule d'Euler.\end{proof}

\begin{proof}[Démonstration du théorème \ref{thm:prolongementGamma}]Fixons $z\in\C$ et soit $G(z) = \lim_{n\to+\infty}\frac{z(z+1)…(z+n)}{n^zn!} = \lim_{n\to+\infty} G_n(z)$.
	\begin{enumerate}
		\item Soit $n\in\N$. On écrit :
		\begin{align*}
			G_n(z) = z\prod_{k=1}^n\underbrace{\left(1+\frac zk\right)e^{z\log\frac{k}{k+1}}}_{\stackrel{\text{def.}}{=} f_k(z)}
		\end{align*}
		Quel que soit $k\in\integerInterval1n$, $f_k\in\mathcal H(\C)$.
		\item Soient $R\in\R^{+*}$ tel que $\abs z<R$. On suppose que $n>R$, de sorte que :
		\begin{align*}
			G_n(z) = z\prod_{k=1}^{\integerPart R} f_k(z)\prod_{k=\integerPart R+1}^n f_k(z)
		\end{align*}
		Lorsque $k>R$, $\frac{\abs z}{k}<1$, donc $f_k(z) = e^{\log(1+\frac zk) - z\log(1+\frac1k)}$. Ainsi :
		\begin{align*}
			G_n(z) = z\prod_{k=1}^{\integerPart R}f_k(z)\cdot\exp\left(\sum_{k=\integerPart R+1}^n\left(\log(1+\frac kz) - z\log(1+\frac1k)\right)\right)
		\end{align*}
		\item Or, il existe $C(R)\in\R^{+*}$ tel que pour tout $k>R, \abs{\log(1+\frac kz) - z\log(1+\frac 1k)} \le \frac{C(R)}{k^2}$. Donc la série de terme général $\sum \log(1+\frac kz) - z\log(1+\frac 1k)$ converge uniformément sur $D(0, R)$ vers une fonction holomorphe. Ainsi, la suite $(G_n)_{n\in\N}$ converge uniformément sur $D(0, R)$, donc $G$ est holomorphe sur $D(0, R)$. Par arbitraire sur $R$, on conclut que $G\in\mathcal H(\C)$.
		\item Quel que soit $z\in\C, G(z) = z\prod_{k=1}^{+\infty}f_k(z)$ et comme $\prod f_k$ converge, les zéros de $G$ sont les zéros des $f_k, k\in\N$. Ainsi, $G$ s'annule sur $\Z^-$. Posons alors $F = \frac 1G\in\mathcal H(\C\setminus \Z^-)$. Par construction,
		\begin{align*}
			\forall z\in\omega, \; F(z) = \lim_{n\to+\infty} \frac{n^zn!}{z(z+1)…(z+n)},
		\end{align*}
		donc $F = \Gamma$ sur $\omega$, donc $F$ prolonge $\Gamma$ de la manière annoncée.
	\end{enumerate}
\end{proof}
