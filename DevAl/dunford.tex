\begin{thm}[Décomposition de Dunford]Soit $f\in\mathcal L(E)$ tel que $\chi_f$ soit scindé. Alors, il existe un unique couple $(d, n)\in\mathcal L(E)\times\mathcal L(E)$ avec $d$ diagonalisable et $n$ nilpotent, tel que $f = d+n$ et $dn=nd$.\end{thm}

\begin{proof}\begin{description}
	\item[Existence]On note $(\lambda_1, …,\lambda_p)\in K^p$ les valeurs propres de $f$ et $(\alpha_1, …, \alpha_p)$ leurs multiplicités respectives. Pour $i\in\integerInterval 1p$, soit $N_{\lambda_i}$ le sous-espace caractéristique associé à $\lambda_i$. Soit $\base b$ une bas de réduction selon les sous-espaces caractéristiques, i.e. :
	\begin{align*}
		\Mat{\base b}{f} &= \begin{pmatrix}
			\lambda_1 & *_1    & *_1       & 0      & …         & …      & 0\\
			0         & \ddots & *_1       & \ddots & \ddots    &        & \vdots\\
			\vdots    & \ddots & \lambda_1 & \ddots & \ddots    &  \ddots& \vdots\\
			\vdots    &        & \ddots    & \ddots & \ddots    &  \ddots& 0 \\
			\vdots    &        &           & \ddots & \lambda_p & *_p    & *_p\\
			\vdots    &        &           &        & \ddots    & \ddots & *_p\\
			        0 & …      & …         & …      & …         & 0      & \lambda_p
		\end{pmatrix}\\ &=
		\underbrace{\begin{pmatrix}
			\lambda_1 & 0      & …         & …      & …         & …      & 0\\
			0         & \ddots & \ddots    &        &           &        & \vdots\\
			\vdots    & \ddots & \lambda_1 & \ddots &           &        & \vdots\\
			\vdots    &        & \ddots    & \ddots & \ddots    &        & 0 \\
			\vdots    &        &           & \ddots & \lambda_p & \ddots & 0  \\
			\vdots    &        &           &        & \ddots    & \ddots & 0  \\
			        0 & …      & …         & …      & …         & 0      & \lambda_p
		\end{pmatrix}}_{D'} +
		\underbrace{\begin{pmatrix}
			0         & *_1    & *_1       & 0      & …         & …      & 0\\
			\vdots    & \ddots & *_1       & \ddots & \ddots    &        & \vdots\\
			\vdots    &        & \ddots    & \ddots & \ddots    &  \ddots& \vdots\\
			\vdots    &        &           & \ddots & \ddots    &  \ddots& 0 \\
			\vdots    &        &           &        & \ddots    & *_p    & *_p\\
			\vdots    &        &           &        &           & \ddots & *_p\\
			        0 & …      & …         & …      & …         & …      & 0 
		\end{pmatrix}}_{N'}
	\end{align*}
	Soit $(d, n)\in\mathcal L(E)\times\mathcal L(E)$ tel que $\Mat{\base b}{d} = D'$ et $\Mat{\base b}{n} = N'$. Par construction, $d$ est diagonalisable et $n$ est nilpotent. Pour tout $i\in\integerInterval 1p$, les matrices $\begin{pmatrix}\lambda_i & &\\ &\ddots & \\ & &\lambda_i\end{pmatrix}$ et $\begin{pmatrix}0 & & *_i\\ &\ddots & \\ & & 0\end{pmatrix}$ sont de même taille $\alpha_i$ et commutent. Donc $D'$ et $N'$ commutent, i.e. $dn = nd$.
	\item[Unicité] Soient $d, d'$ deux endomorphismes diagonalisables et $n, n'$ deux endomorphismes nilpotents tels que $\left\{\begin{aligned}d+n &= d'+n' = f\\d'n' &= n'd'\end{aligned}\right.$. Montrons que $d=d'$ et que $n=n'$. Soit $h = d-d' = n-n'$.
	\begin{itemize}
		\item $d$ et $n$ commutent, donc $d$ et $d+n$ commutent. Autrement dit, $d$ et $f$ commutent. De même, $d'$ et $f$ commutent, puis $fn' = n'f$ et $nf = fn$.
		\item On écrit $E = \oplus_{i=1}^p N_{\lambda_i}$. Pour $i\in\integerInterval 1p$, soit $q_i:E\to E$ le projecteur sur $N_{\lambda_i}$. Dans la base $\base b$,
		\begin{align*}
		\Mat{\base b}{q_i} = \begin{pmatrix}
			0 & 0      & …      & …       & …      & …      & …      & …      &\\
			0 & \ddots & \ddots &         &        &        &        &        &\vdots\\
			\vdots & \ddots & 0      & \ddots  &        &        &        &        &\vdots\\
			\vdots &        & \ddots & 1       & \ddots &        &        &        &\vdots\\
			\vdots &        &        & \ddots  & \ddots & \ddots &        &        &\vdots\\
			\vdots &        &        &         & \ddots & 1      & \ddots &        &\vdots\\
			\vdots &        &        &         &        & \ddots & 0      & \ddots &\vdots\\
			\vdots &        &        &         &        &        & \ddots & \ddots & 0\\
			0      & …      & …      & …       & …      & …      & …      &    0 & 0
		\end{pmatrix}\end{align*}
		Donc $d = \lambda_1q_1 + … + \lambda_pq_p$.
		\item Montrons que $d'$ stabilise tous les sous-espaces caractéristiques. Soient $i\in\integerInterval 1p$ et $x\in N_{\lambda_i}$. Alors, $(f - \lambda_i\Id)^{\alpha_i}(x) = 0$. Or, $d'f = fd'$, donc :
		\begin{align*}
			(f - \lambda_i\Id)^{\alpha_i}\circ d'(x) = d'\circ(f - \lambda_i\Id)^{\alpha_i}(x) = d'(0) = 0
		\end{align*}
	Donc $d'(x) \in N_{\lambda_i}$.
	\item Montrons que $d'$ commute avec tous les $q_i, 1\le i\le p$. Soient $i\in\integerInterval 1p$ et $x\in E$. On écrit $x = \sum_{k=1}^p x_k$, avec $x_k\in N_{\lambda_k}$. Ainsi :
	\begin{align*}\left\{
		\begin{aligned}
			&d'\circ q_i(x) = d'(x_i)\\
			&q_i\circ d'(x) = q_i\circ d'(x_1 + … + x_p) = q_i\circ d'(x_1) + … + q_i\circ d'(x_p) = d'(x_i)
			\end{aligned}
		\right.\end{align*}
		Donc $d'\circ q_i = q_i\circ d'$.
		\item Or, $d = \lambda_1q_1 + … + \lambda_pq_p$, donc $d'd = dd'$. Enfin, $n'd = (f-d')d = df-dd' = dn'$.
		\item La somme de deux endomorphismes diagonalisables qui commutent est diagonalisable, donc $(d-d')$ est diagonalisable. De même, la somme de deux endomorphismes nilpotents qui commutent est un endomorphisme nilpotent. Donc $(n-n')$ est nilpotent. Au final, $h = d-d' = n-n'$ est diagonalisable et nilpotent donc est nul.
	\end{itemize}
\end{description}\end{proof}
