\begin{definition}[Fonction de Möbius]Soit $\mu:\N^*\to\N$ définie par :
	\begin{align*}
		\mu(n) = \begin{cases}
			0  & \text{si $n$ est divisible par le carré d'un entier}\\
			-1 & \text{si $n$ est le produit d'un nombre pair de nombre premiers distincts}\\
			1  & \text{sinon}
		\end{cases}
	\end{align*}
\end{definition}

\begin{lemme}[Formule d'inversion de Möbius]\begin{enumerate}
	\item Si $m, n$ sont deux entiers premiers entre eux, $\mu(mn) = \mu(m)\mu(n)$.
	\item\label{mobius1} $\forall n\in\N^*, \; \sum_{d|n}\mu(d) = \begin{cases}1 &\text{si }n=1\\0&\text{sinon}\end{cases}$. 
	\item Soient $f:\N^*\to\R$ et $g:\anonymfunction{\N^*}{\R}{n}{\sum_{d|n}f(d)}$. Alors :
	\begin{align*}
		\forall n\in\N^*, \; f(n) = \sum_{d|n}\mu\left(\frac nd\right)g(d) = \sum_{d|n}\mu(d)g\left(\frac nd\right)
	\end{align*}
\end{enumerate}
\end{lemme}

\begin{proof}\begin{enumerate}
	\item Si $n=1$ ou $m=1$, il n'y a rien à démontrer. Si $m$ ou $n$ admet un facteur carré, $mn$ aussi. Sinon, $n$ s'écrit $n = p_1…p_r$ et $m$ s'écrit $m = q_1…q_s$ où $p_1, …, p_r, q_1, …, q_s$ sont des nombres premiers distincts. Alors :
	\begin{align*}
		\mu(nm) = (-1)^{r+s} = (-1)^r(-1)^s = \mu(n)\mu(m)
	\end{align*}
	\item Le cas $n=1$ est évident. Supposons $n>1$, et décomposons $n$ en facteurs premiers : $n = p_1^{\alpha_1}…p_r^{\alpha_r}$. Alors :
	\begin{align*}
		\sum_{d|n}\mu(d) &= \mu(1) + \sum_{i=1}^r\mu(p_i) + \sum_{1\le i<j\le r}\mu(p_ip_j) + … + \mu(p_1…p_r)\\
		  &= 1 - \sum_{i=1}^r + \sum_{1\le i<j\le r} 1 + … + (-1)^r\\
		  &= 1 - \binom r1 + \binom r1 + … + (-1)^r\\
		  &= (1-1)^r = 0
	\end{align*}
	\item Soit $n\in\N^*$. On calcule :
	\begin{align*}
		&\sum_{d|n}\mu(d)g\left(\frac nd\right) = \sum_{d|n}\mu(d)\left(\sum_{d'|\frac nd}f(d')\right)\\
		  = & \sum_{dd'|n}\mu(d)f(d')
		  = \sum_{d'|n}f(d')\left(\sum_{d|\frac{n}{d'}}\mu(d)\right)\\
		  \stackrel{\eqref{mobius1}}{=} & f(n)
	\end{align*}
\end{enumerate}\end{proof}

\begin{thm}Soient $q$ une puissance d'un nombre premier et $n\in\N^*$. On note $A(n, q)$ l'ensemble des polynômes de degré $n$ unitaires irréductibles sur $\F_q$ et $I(n, q) = \card{A(n, q)}$. Alors :
	\begin{align}
		\left\{\begin{aligned}
			&I(n, q) \asymptequiv_{n\to+\infty} \frac{q^n}{n}\\
			&\forall n\in\N^*, \; I(n, q) \ge 1
		\end{aligned}\right.\label{eq:nbPolFq1}
	\end{align}
\end{thm}

\begin{proof}\begin{enumerate}
	\item Soient $d$ un diviseur de $n$ et $P\in A(d, q)$. Montrons que $P|X^{q^n}-X$.
	
	Soit $\pi:\anonymfunction{\F_q[X]}{\F_q[X]/(P)}{X}{\overline X}$. Posons $\beta = \pi(X) = \overline X$. Par définition, $\F_q[X]/(P) \simeq \F_{q^d}$, donc $\beta^{q^d}=\beta$. Or, $d|n$, donc $\beta^{q^n}=\beta$. Donc $P|X^{q^n}-X$.
	\item Soit $P$ un facteur unitaire irréductible de $X^{q^n}-X$. Montrons que $\deg P|n$. Soit $d = \deg P$. $X^{q^n}-X$ est scindé sur $\F_{q^n}$. Soit donc $x\in\F_{q^n}$ une racine de $P$. Soit $K = \F_q(x)$. Par construction, $\F_q(x)$ est une extension de corps de $\F_q$ de degré $d$ et est un sous-corps de $\F_{q^n}$. Par multiplicité des degrés, $[\F_{q^n}:K][K:\F_q] = n$, i.e. $d|n$.
	\item\begin{itemize}
		\item Dans $\F_{q^n}$, toutes les racines de $X^{q^n}-X$ sont simples. Donc les facteurs irréductibles de $X^{q^n}-X$ dans $\F_q[X]$ sont tous de multiplicité $1$. On déduit des deux étapes précédentes que :
		\begin{align*}
			X^{q^n}-X = \prod_{d|n}\prod_{P\in A(d, q)} P
		\end{align*}
		D'où, en passant aux degrés :
		\begin{align}\label{eq:nbPolFq2}
			q^n = \sum_{d|n}\deg\left(\prod_{P\in A(d, q)} P\right) = \sum_{d|n}\sum_{P\in A(d, q)} \deg P = \sum_{d|n} d\cdot I(d, q)
		\end{align}
		\item Pour $n\in\N^*$, posons $g(n) = \sum_{d|n}d\cdot I(d, q)$ et pour $d\in\N^*$, posons $f(d) = d\cdot I(d, q)$. D'après la formule d'inversion de Möbius,
		\begin{align*}
			f(n) &= n\cdot I(n, q) = \sum_{d|n}\mu\left(\frac nd\right)g(d)\\
			     &= \sum_{d|n}\left(\mu\left(\frac nd\right) \underbrace{\left(\sum_{d'|d}d'\cdot I(d', d) \right)}_{ \stackrel{\eqref{eq:nbPolFq2}}{=} q^d} \right)\\
			     &= \sum_{d|n} \mu\left(\frac nd\right)q^d
		\end{align*}
		En divisant par $n$, on obtient $I(n, q) \stackrel{(*)}{=} \frac 1n\sum_{d|n}\mu\left(\frac nd\right)q^d$.
	\end{itemize} 
	\item Montrons \eqref{eq:nbPolFq1}. Posons :
	\begin{align*}
		r_n = \sum_{d|n, d<n}\mu\left(\frac nd\right)q^d
	\end{align*}
	Ainsi, $I(n, q) = \frac 1n(q^n + r^n)$. Alors :
	\begin{align*}
		\abs{r_n} &\le \sum_{d=1}^{\integerPart{\frac n2}} q^d = q\cdot\frac{q^{\integerPart{\frac n2}}-1}{q-1} \qquad \text{car } q\not=1\\
		  &\le \frac{q^{\integerPart{\frac n2 + 1}}}{q-1}
	\end{align*}
	Donc $\abs{r^n} = o(q^n)$ lorsque $n\to+\infty$. D'après $(*)$, on déduit l'équivalent asymptotique annoncé.
	
	Enfin, pour tout $n\in\N^*$,
	\begin{align*}
		\abs{r_n} \le \frac{q^{\integerPart{\frac n2}+1}}{\frac q2} \le 2q^{\integerPart{\frac n2}} < q^n
	\end{align*}
	Et donc $I(n, q) \ge \frac 1n(q^n - \abs{r^n}) > 1$.
\end{enumerate}\end{proof}
