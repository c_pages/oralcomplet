\begin{prop}Soient $\alpha_1, …, \alpha_p$ des entiers naturels non nuls premiers entre eux dans leur ensemble. Pour tout $n\in\N$, soit $S_n$ le nombre de solutions $(n_1, …, n_p)\in\N^p$ de l'équation :
	\begin{align}\label{eq:nbSolEqEnonce}
		\alpha_1 n_1 + … + \alpha_p n_p = 1
	\end{align}
Alors :
	\begin{align*}
		S_n \asymptequiv_{n\to+\infty} \frac{1}{\alpha_1 \cdot…\cdot \alpha_p} \frac{n^{p-1}}{(p-1)!}
	\end{align*}
\end{prop}

\begin{proof}\begin{itemize}
	\item Pour $z\in D(0, 1)$, soit :
	\begin{align*}
		F(z) &= \left(\sum_{n_1=0}^{+\infty}z^{\alpha_1 n_1}\right) … \left(\sum_{n_p=0}^{+\infty}z^{\alpha_p n_p}\right)\\
		     &= \sum_{n=0}^{+\infty}\left(\sum_{n_1+…+n_p=n}z^{\alpha_1 n_1}…z^{\alpha_p n_p}\right)\\
		     &= \sum_{n=0}^{+\infty} S_nz^n \qquad \text{(Produit de Cauchy)}\\
		     &= \frac{1}{1-z^{\alpha_1}} \cdot…\cdot \frac{1}{1-z^{\alpha_p}}
	\end{align*}
	\item $\anonymfunction{D(0,1)}{\C}{z}{F(z)}$ est une fraction rationnelle, dont les pôles sont les racines $\alpha_1$-ièmes, …, $\alpha_p$-ièmes de l'unité. On note $\Pi$ leur ensemble.
	\begin{itemize}
		\item On remarque que $\mult_{\Pi}(1) = p$.
		\item Montrons que si $\omega\in\Pi\setminus\{1\}$, alors $\mult_\Pi(\omega)<p$. Soit $\omega = e^{\frac{2ia\pi}{b}}$ avec $\pgcd(a, b) = 1$ un élément de $\Pi$. Alors, $\omega^{\alpha_1} = … = \omega^{\alpha_p} = 1$. Donc $b|\alpha_1 a, …, \alpha_p a$, donc $b|1$ donc $b = 1$. Donc $\mult_\pi(\omega) < p$.
	\end{itemize}
	La décompositions en éléments simples de $F$ s'écrit :
	\begin{align}\label{eq:nbSolEq1}
		\forall z\in D(0, 1), \; F(z) = \frac{A}{(1-z)^p} + G(z),
	\end{align}
	où :
	\begin{align*}
		\left\{\begin{aligned}
			&G(z) = \sum_{\omega\in\Pi}\left(\frac{a_{1,\omega}}{\omega-z} + … + \frac{a_{p-1,\omega}}{(\omega-z)^{p-1}}\right), \quad (\alpha_{1,\omega}, …, \alpha_{p-1,\omega})\in\C^{p-1}\\
			&A\in\C
		\end{aligned}\right.
	\end{align*}
	\item Soit $z\in D(0, 1)$. On multiplie \eqref{eq:nbSolEq1} par $(1-z)^p$, ce qui donne $(1-z)^pF(z) \stackrel{\eqref{eq:nbSolEq1}}{=} A + (1-z)^p G(z)$. D'autre part :
	\begin{align*}
		(1-z)^pF(z) &= (1-z)^p\left(\frac{1}{1-z^{\alpha_1}} \cdot…\cdot \frac{1}{1-z^{\alpha_p}}\right)\\
		  &= \frac{1}{1+z+…+z^{\alpha_1-1}} \cdot…\cdot \frac{1}{1+z+…+z^{\alpha_p-1}}
	\end{align*}
	En prenant $z = 1$, il vient $A = \frac{1}{\alpha_1\cdot…\cdot\alpha_p}$.
	\item Soient $\omega\in\Pi$ et $z\in D(0, 1)$. En dérivant $k\in\N^*$ fois $\frac{1}{\omega-z}$, on trouve :
	\begin{align*}
		\frac{1}{(\omega-z)^k} = \frac{1}{(k-1)!}\sum_{n=0}^{\infty}\underbrace{\frac{(n+k-1)!}{n!}\omega^{-n-k}}_{= O(n^{k-1})}z^n
	\end{align*}
	Or, $\abs\omega=1$, donc :
	\begin{align*}
		G(z) = \sum_{\omega\in\Pi}\left(\sum_{n=0}^{+\infty}\left(a_{1,\omega}O(1) + … + a_{p-1,\omega}O(n^{p-1})\right)z^n\right)
	\end{align*}
	Autrement dit,
	\begin{align}\label{eq:nbSolEq2}
		G(z) = \sum_{n=0}^{+\infty}\left(\sum_{\omega\in\Pi} O(n^{p-2})\right)z^n
	\end{align}
	\item Enfin, pour tout $z\in D(0, 1)$,
	\begin{align*}
		\frac{A}{(1-z)^p} &= \frac{A}{(p-1)!}\sum_{n=0}^{+\infty} \frac{(n+p-1)!}{n!}z^n\\
		  &= \frac{A}{(p-1)!}\sum_{n=0}^{+\infty}(n+1)\cdot…\cdot(n+p-1)z^n
	\end{align*}
	D'après \eqref{eq:nbSolEq1} et \eqref{eq:nbSolEq2},
	\begin{align*}
		F(z) &= \sum_{n=0}^{+\infty}\left(\frac{A}{(p-1)!} (n+1)\cdot…\cdot(n+p-1) + O(n^{p-2})\right)z^n\\
		  &= \sum_{n=0}^{+\infty} S_nz^n
	\end{align*}
	Donc, pour tout $n\in\N$, $S_n = \frac{A}{(p-1)!}(n+1)\cdot…\cdot(n+p-1) + P(n^{p-2})$. Donc :
	\begin{align*}
		S_n \asymptequiv_{n\to+\infty} \frac{1}{\alpha_1\cdot…\cdot\alpha_p} \frac{n^{p-1}}{(p-1)!}
	\end{align*}
\end{itemize}\end{proof}
