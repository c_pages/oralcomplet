\begin{definition}Un ellipsoïde centré en $0$ est un ensemble $\mathcal E_q = \setst{x\in\R^n}{q(x)\le 1}$, où $q \in Q_{++}$. On note $V_q$ le volume de $\mathcal E_q$.\end{definition}

\begin{lemme}\label{lem:convLogDet}Soient $(A, B)\in S^n_{++}(\R)\times S^n_{++}(\R)$ et $(\alpha, \beta)\in\R^+\times\R^+$ tel que $\alpha+\beta = 1$. Alors, $\det(\alpha A + \beta B) \ge (\det A)^\alpha(\det B)^\beta$.\end{lemme}

\begin{proof}[Démonstration du lemme \ref{lem:convLogDet}]Par le théorème de pseudo-réduction simultanée, il existe $P\in\GL_n(\R)$ et $D = \Diag(\lambda_1, …, \lambda_n)$ une matrice diagonale réelle à diagonale strictement positive, telles que $A = ^t\!P P$ et $B = ^t\!PDP$. On calcule :
	\begin{align*}
		(\det A)^\alpha(\det B)^\beta &= (\det(^t\!PP))^\alpha(\det(^t\!PDP))^\beta\\
		                              &= (\det P)^\alpha(\det P^2)\beta(\det B)^\beta\\
		                              &= (\det P)^2(\det D)^\beta
	\end{align*}
Or, $\det(\alpha A + \beta B) = \det(P^2)\det(\alpha\Id + \beta D)$. Il s'agit donc de montrer que $\det(\alpha\Id + \beta B) \ge (\det D)^\beta$. On écrit les équivalences suivantes :
	\begin{align*}
		\det(\alpha\Id + \beta D) \ge (\det D)^\beta &\iff \prod_{i=1}^n(\alpha+\beta\lambda_i) \ge \left(\prod_{i=1}^n\lambda_i\right)^\beta\\
		  &\iff \sum_{i=1}^n \ln(\alpha+\beta\lambda_i) \ge \beta\sum_{i=1}^n \ln\lambda_i
	\end{align*}
	La concavité du logarithme assure que la dernière inégalité est vraie, ce qui démontre le lemme annoncé.\end{proof}


\begin{thm}Soit $K\subset\R^n$ un compact d'intérieur non vide. Il existe un unique ellipsoïde centré en $0$, de volume minimal, et contenant $K$.\end{thm}

\begin{proof}On munit $\R^n$ de sa structure euclidienne canonique.
	\begin{enumerate}
		\item Soit $q\in Q_{++}$. Calculons $V_q$. Il existe $\base b$ une base de $\R^n$ telle que :
		\begin{align*}
			\forall x\in\R^n, \; q(x) = \sum_{i=1}^n a_ix_i^2,
		\end{align*}
avec $a_i>0$ pour tout $i\in\integerInterval 1n$, car $q$ est définie positive. Donc :
		\begin{align*}
			V_q &= \int…\int_{a_1x_1^2 + … + a_nx_n^2 \le 1}dx_1…dx_n\\
			    &= \int…\int_{t_1^2 + … + t_n^2\le 1}\frac{1}{\sqrt{\det q}}dt_1…dt_n\\
			    &= \frac{V_0}{\sqrt{\det q}},
		\end{align*}
		où $V_0$ est le volume de la boule unité de $\R^n$.
		\item On se ramène à chercher $q\in Q_{++}$ de déterminant maximal, telle que pour tout $x\in K, q(x)\le 1$. On définit :
		\begin{align*}
			\left\{\begin{aligned}
				&\forall q\in Q, \; N(q) = \sup_{\norm x\le 1}\abs{q(x)}\\
				& \mathcal A = \setst{q\in Q_{+}}{\forall x\in K, q(x)\le 1}
			\end{aligned}\right.
		\end{align*}
		On cherche donc $\max_{q\in\mathcal A}(\det q)$. Montrons que $\mathcal A$ est non vide, convexe et compact dans $Q$.
		\begin{description}
			\item[Non vide]$K$ est compact donc borné par un $M\in\R^{+*}$. On définit $q:\anonymfunction{K}{\R^+}{x}{\frac{\norm x^2}{M^2}}$. Ainsi, $q\in\mathcal A$.
			\item[Convexité] Soient $(q, q')\in\mathcal A$ et $\lambda\in\intervalCC 01$. Pour tout $x\in\R^n$,
			\begin{align*}
				(\lambda q + (1-\lambda)q')(x) = \lambda q(x) + (1-\lambda)q'(x) \ge 0
			\end{align*}
			Si de plus $x\in K$, alors :
			\begin{align*}
				(\lambda q(x) + (1-\lambda)q'(x)) = \lambda q(x) + (1-\lambda)q'(x) \le \lambda + (1-\lambda) = 1
			\end{align*}
			Donc $\lambda q + (1-\lambda)q'\in\mathcal A$.
			\item[Fermeture]Soit $(q_n)_{n\in\N}\in\mathcal A^\N$ une suite de limite $q\in Q$. Montrons que $q\in\mathcal A$. Si $x\in\R^n$, alors $\abs{q(x) - q_n(x)} \le N(q-q_n)\norm x$, donc $\lim_{n\to+\infty}q_n(x) = q(x)$. Ainsi :
			\begin{align*}
				\left\{\begin{aligned}
					&\forall x\in\R^n, \; q(x) = \lim_{n\to+\infty} q_n(x) \ge 0\\
					&\forall x\in K, \; q(x)  = \lim_{n\to+\infty} q_n(x) \le 1
				\end{aligned}\right.
			\end{align*}
			Donc $q\in\mathcal A$.
			\item[Borné]$K$ étant d'intérieur non vide, il existe $r\in\R^{+*}$ et $a\in K$ tels que $\bowl a r\subset K$. Soit $q\in\mathcal A$. Lorsque $x\in\bowl 0r, x+a\in\bowl a r$ donc $q(x+a)\le 1$. En outre, $q(-a) = q(a)$, et l'inégalité de Minkowski donne :
			\begin{align*}
				\sqrt{q(x)} = \sqrt{q(x+a-a)} \le \sqrt{q(x+a)} + \sqrt{q(-a)} \le 2
			\end{align*}
			Donc $q(x)\le 4$. Enfin, si $x\in\bowl 01$, alors $\abs{q(x)} = \frac{1}{r^2}q(rx) \le \frac{4}{r^2}$ donc $N(q) \le \frac{4}{r^2}$.
		\end{description}
		$\mathcal A$ est donc un convexe compact non vide de $Q$. Donc l'application $\det$ admet un maximum sur $\mathcal A$, atteint en un $q_0\in\mathcal A$. Comme $x\mapsto\frac{\norm x^2}{M^2}$ est définie positive et est dans $\mathcal A$, $q_0$ est aussi définie positive i.e. $\det q_0\not=0$. Ceci montre l'existe d'un ellipsoïde ayant les propriétés attendues.
		\item Démontrons l'unicité. Soit $q\in\mathcal A$ tel que $\det q = \det q_0$. Soient $S$ et $S_0$ les matrices respectives de $q$ et $q_0$ dans la base canonique de $\R^n$. Par convexité de $\mathcal A$, $\frac 12(q+q_0)\in\mathcal A$. D'après le lemme \ref{lem:convLogDet},
		\begin{align*}
			\det\left(\frac 12(q+q_0)\right) &= \det\left(\frac12(S+S_0\right)\\
			  &\ge (\det S)^{\frac12}(\det S_0)^{\frac12}\\
			  &\ge \det S_0\\
			  &= \det q_0,
		\end{align*}
		donc $q_0 = q$.
	\end{enumerate}
\end{proof}
