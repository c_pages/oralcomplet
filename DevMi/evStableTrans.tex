\begin{lemme}Soient $n\in\N^*$ et $(f_1, …, f_n)$ une famille libre de fonctions de $\R\to\R$. Il existe $(x_1, …, x_n)\in\R^n$ tel que la matrice $M = (f_i(x_j))_{1\le i,j\le n}$ soit inversible.\end{lemme}

\begin{proof}Soit $\base b = (f_1, …, f_n)$ et $F = \Vect\base b$. Puisque $\base b$ est libre, $\dim F = n$. Pour tout $a\in\R$, on note $e_a:\anonymfunction{F}{\R}{f}{f(a)}\in F^*$. On note $A = \Vect(\setst{e_a}{a\in\R})$. Alors :
	\begin{align*}
		\forall f\in A^\circ, \forall a\in \R, \; e_a(f) = f(a) = 0
	\end{align*}
	Donc $A^\circ = \{0_F\}$. Donc $\Vect A = (\Vect A^\circ)^\perp = (A^\circ)^\perp = F$, car $F$ est de dimension finie. Il existe donc $(x_1, …, x_n)\in\R^n$ tel que la famille $(e_{x_1}, …, e_{x_n})$ forme une base de $F^*$. Notons alors $M = (f_i(x_j))_{1\le i,j\le n}$. Montrons que $M$ est inversible.
	
	Soient $L_1, …, L_n$ les lignes de $M$ ; montrons que la famille $(L_1, …, L_n)$ est libre. Soit $(\lambda_1, …, \lambda_n)\in\R^n$ tel que :
	\begin{align*}
		\sum_{i=1}^n \lambda_i L_i = 0
	\end{align*}
	Alors, quel que soit $j\in\integerInterval 1n$,
	\begin{align*}
		&\sum_{i=1}^n \lambda_if_i(x_j) = 0\\
		\text{i.e.}\quad & e_{x_j}\left(\sum_{i=1}^n\lambda_if_i\right) = 0
	\end{align*}
	Par suite, $\sum_{i=1}^n\lambda_if_i\in (F^*)^\circ$, avec $(F^*)\circ = \{0\}$. Or, la famille $(f_1, …, f_n)$ est une base, donc $\lambda_1= … = \lambda_n = 0$. Donc $M\in\GL_n(\R)$.\end{proof}

\begin{prop}Pour tous $f\in\mathcal F(\R, \R)$ et  $a\in\R$, on note $f_a:\anonymfunction{\R}{\R}{x}{f(x+a)}$. Soit $f\in\mathcal C^0(\R,\R)\cap\mathcal D(\R, \R)$. Alors, $\Vect(\setst{f_a}{a\in\R})$ est de dimension finie si et seulement si $f$ est la solution d'une équation différentielle linéaire homogène à coefficients constants.\end{prop}

\begin{proof}\begin{description}
	\item[$\Leftarrow$]Supposons que $f$ est solution d'une équation différentielle linéaire homogène d'ordre $p\in\N$ à coefficients constants. Alors, pour tout réel $a$, $f_a$ est solution de la même équation et donc $\Vect(\setst{f_a}{a\in\R})$ est de dimension au plus $p$.
	\item[$\Rightarrow$]Soit $f$ une fonction continue dérivable de $\R$ dans $\R$, telle que $F = \Vect(\setst{f_a}{a\in\R})$ soit de dimension finie $n\in\N^*$. Soit $(a_1, …, a_n)\in\R^n$ tel que $(f_{a_1}, …, f_{a_n})$ soit une base de $F$. D'après le lemme, il existe $(x_1, …, x_n)\in\R^n$ tel que la matrice $M = (f_i(x_j))_{1\le i,j\le n}$ soit inversible.
	
	La fonction $f$ est dérivable, donc pour tout $i\in\integerInterval 1n$, $f_{a_i}$ est dérivable. Donc $F\subset\mathcal D(\R, \R)$. Fixons $g\in F$ et montrons que $g'\in F$.
	\begin{itemize}
		\item Soit $a\in\R$. Alors, $g_a\in\Vect(f_{a_1+a}, …, f_{a_n+a})$, donc il existe $(\lambda_1(a), …, \lambda_n(a))\in\R^n$ tel que :
		\begin{align*}
			g_a = \sum_{i=1}^n\lambda_i(a)f_{a_i}
		\end{align*}
		\item Pour tout $j\in\integerInterval 1n$,
		\begin{align*}
			g(a+x_j) = \sum_{i=1}^n \lambda_i(a)f_{a_i}(x_j)
		\end{align*}
		Notons alors $M = (f_{a_i}(x_j))_{1\le i,j\le n}$, de sorte que :
		\begin{align*}
			(g(a+x_j))_{1\le j\le n} =  ^t\! M \begin{pmatrix}\lambda_1(a)\\\vdots\\ \lambda_n(a)\end{pmatrix}
		\end{align*}
		Ainsi,
		\begin{align*}
			\begin{pmatrix}\lambda_1(a)\\\vdots\\\lambda_n(a)\end{pmatrix} = (^t\!M)^{-1}\begin{pmatrix}g(a+x_1)\\\vdots\\g(a+x_n)\end{pmatrix}
		\end{align*}
		\item Les fonctions $\lambda_1, …, \lambda_n$ sont donc des combinaisons linéaires de $g_{x_1}, …, g_{x_n}$ donc sont dérivables.
		\item On sait que pour tout $x\in\R$,
		\begin{align*}
			g(x+a) = \sum_{i=1}^n \lambda_i(a)f_{a_i}(x)
		\end{align*}
		Dérivant cette relation par rapport à $a$, on déduit :
		\begin{align*}
			g'(x+a) = \sum_{i=1}^n \lambda_i'(a)f_{a_i}(x)
		\end{align*}
		En $a = 0$, on conclut enfin que :
		\begin{align*}
			g' = \sum_{i=1}^n \lambda_i'(a)f_{a_i}(x) \in F
		\end{align*}
	\end{itemize}
	Ainsi, $F\subset\mathcal C^{\infty}(\R, \R)$ et pour tout $k\in\N, g^{(k)}\in F$. En particulier, $f$ est de classe $\mathcal C^\infty$ et toutes les dérivées de $f$ appartiennent à $F$, qui est de dimension finie $n$. Il existe donc un entier naturel $p$ tel que $f^{(p)}\in\Vect((f^{(k)})_{0\le k\le p})$, d'où le résultat annoncé.
\end{description}\end{proof}




